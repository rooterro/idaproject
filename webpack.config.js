const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "development",

  module: {
    rules: [

      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      // images
      {
        test: /\.(png|jpg|gif|ico)$/,
        // use: [{
        //   loader: 'file-loader',
        //   options: {
        //     outputPath: 'images',
        //     name: '[name]-[sha1:hash:7].[ext]'
        //   }
        // }]
        type: 'asset/resource',
      },
      // fonts
      {
        test: /\.(ttf|otf|eot|woff|woff2)$/,
        use: [{
          loader: 'file-loader',
          options: {
            outputPath: 'fonts',
            name: '[name].[ext]'
          }
        }]
      },

      // css
      {
        test: /\.(css)$/,
        use: [
          // {loader: 'style-loader'},
          {loader: MiniCssExtractPlugin.loader},
          {loader: 'css-loader'}
        ]
      },

      // scss
      {
        test: /\.(scss)$/,
        use: [
          // {loader: 'style-loader'},
          {loader: MiniCssExtractPlugin.loader},
          {loader: 'css-loader'},
          {loader: 'sass-loader'},
        ]
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html'
    }),

    new MiniCssExtractPlugin({
      filename: 'main-[hash:8].css'
    }),

    new CopyPlugin({
      patterns: [
        { from: "public/img", to: "img" },
      ],
    }),
  ],

  entry: {
    index: './src/index.js',
  },

  output: {
    clean: true,
    assetModuleFilename: 'src/assets/images/[name].[ext]'
  },
  
}
import './scss/normalize.scss'

import './scss/layout/common.scss'
import './scss/layout/header.scss'
import './scss/layout/main.scss'

import './scss/components/sidebar.scss'
import './scss/components/buttons.scss'
import './scss/components/form.scss'

import './scss/components/card.scss'